public class P1E06 {

	public static void main(String[] args) {
		// P1E06 - EXERCIO 06 DA PARTE 01 DA APOSTILA PAG 50
		double raio, perimetro, area;
		raio      = 1.5;
		perimetro = 2.0 * 3.14159 * raio;
		area      = 3.14159 * raio * raio;
		System.out.println("Perimetro = " + perimetro);
		System.out.println("Area      = " + area);
	}
}